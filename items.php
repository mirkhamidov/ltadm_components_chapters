<?
$items=array(
  "name" => array(
         "desc" => "Название раздела меню",
         "type" => "text",
         "maxlength" => "255",
         "size" => "30",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),
  "url" => array(
         "desc" => "Идентификатор меню",
         "type" => "text",
         "maxlength" => "255",
         "size" => "50",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и '_','-'!",
           ),
         ),
         "select_on_edit" => true,
       ),
  "orders" => array(
         "desc" => "Порядок вывода в списках",
         "type" => "order",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[\d]+$",
             "flags" => "g",
             "error" => "Только цифры! От 0 до 99999!",
           ),
         ),
       ),
);
?>