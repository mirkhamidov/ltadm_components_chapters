<?
$config=array(
    "name"      => "Разделы сайта",
    "menu_icon" => "icon-tag",
    "status"    => "system",
    // "windows"   => array(
    //     "create" => array("width" => 500,"height" => 300),
    //     "edit"   => array("width" => 600,"height" => 300),
    //     "list"   => array("width" => 600,"height" => 500),
    // ),
    "right"      => array("admin","#GRANTED"),
    "main_table" => "lt_menu",
    "list"       => array(
        "name"   => array("isLink" => true),
        "url"    => array("align" => "center"),
        "orders" => array("align" => "center"),
    ),
    "select" => array(
        "default_orders" => array(
            array("orders" => "ASC"),
            // array("name" => "ASC"),
        ),
        "default" => array(
            "id_lt_menu" => array(
                "desc"       => "Раздел меню",
                "type"       => "select_from_table",
                "table"      => "lt_menu",
                "key_field"  => "id_lt_menu",
                "fields"     => array("name"),
                "show_field" => "%1",
                "condition"  => "",
                "order"      => array ("orders" => "ASC"),
                "use_empty"  => true,
            ),
        ),
    ),
    "downlink_tables" => array(
        "lt_pages" => array(
            "key_field" => "id_lt_menu",
            "name" => "name",
            "component" => "pages"
        ),
    ),
);

$actions=array(
    "create" => array(
        "before_code" => "",
    ),
);
